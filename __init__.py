from collections import OrderedDict
import contextlib
import datetime as dt
import logging
import time
import serial
import warnings
import threading
import socket
import os.path
import json

from flatland import Integer, Float, Form, Enum, Boolean
from flatland.validation import ValueAtLeast, ValueAtMost
from microdrop.app_context import get_app
from microdrop.plugin_helpers import AppDataController, StepOptionsController
from microdrop.plugin_manager import (IPlugin, Plugin, implements, emit_signal,
                                      get_service_instance_by_name,
                                      ScheduleRequest) # ,PluginGlobals)
# import from `pyutilib.component.core` directly
from pyutilib.component.core import ExtensionPoint, PluginGlobals

from pygtkhelpers.gthreads import gtk_threadsafe
from pygtkhelpers.ui.extra_dialogs import yesno, FormViewDialog
from pygtkhelpers.ui.objectlist import PropertyMapper
from pygtkhelpers.utils import dict_to_form
from scipy.optimize import curve_fit
import gobject
import gtk
import microdrop_utility as utility
import zika_box_peripheral_board as zikabox
import zika_box_peripheral_board.ui.gtk.monitor_dialog as monitor_dialog
import openpyxl as ox
import openpyxl_helpers as oxh
import numpy as np
import pandas as pd
import path_helpers as ph
import trollius as asyncio

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions


logger = logging.getLogger(__name__)


# Add plugin to `"microdrop.managed"` plugin namespace.
PluginGlobals.push_env('microdrop.managed')

# Path to Zika data collection template Excel spreadsheet workbook.
TEMPLATE_FOLDER = (ph.path(__file__).realpath().parent).joinpath('templates')
TEMPLATE_PATH = TEMPLATE_FOLDER.joinpath('Zika_Data_Collection-named_ranges.xlsx')

def _write_results(template_path, output_path, dropbot_remote):
    '''
    Write results as Excel spreadsheet to output path based on template.

    .. versionadded:: 0.19

    Parameters
    ----------
    template_path : str
        Path to Excel template spreadsheet.
    output_path : str
        Path to write output Excel spreadsheet to.
    data_files : list
        List of paths to `new-line delimited JSON files <http://ndjson.org/`_
        containing measured PMT data.

        Each new-line delimited JSON file corresponds to protocol step, and
        each line in each file corresponds to measured PMT data from a single
        measurement run.

    Returns
    -------
    path_helpers.path
        Wrapped output path.

        Allows, for example, easy opening of document using the ``launch()``
        method.
    '''
    output_path = ph.path(output_path)

    # XXX `openpyxl` does not currently [support reading existing data
    # validation][1].
    #
    # As a workaround, load the extension lists and data validation definitions
    # from the template workbook so they may be restored to the output workbook
    # after modifying with `openpyxl`.
    #
    # [1]: http://openpyxl.readthedocs.io/en/default/validation.html#validating-cells
    extension_lists = oxh.load_extension_lists(template_path)
    data_validations = oxh.load_data_validations(template_path)

    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', 'Data Validation extension is not '
                                'supported and will be removed', UserWarning)

        # Open template workbook to modify it in-memory before writing to the
        # output file.
        with contextlib.closing(ox.load_workbook(template_path)) as workbook:
            # Create pandas Excel writer to make it easier to append data
            # frames as worksheets.
            with pd.ExcelWriter(output_path,
                                engine='openpyxl') as output_writer:
                # Configure pandas Excel writer to append to template workbook
                # contents.
                output_writer.book = workbook

                # Get mapping from each worksheet name to the correpsonding
                # worksheet object.
                worksheets_by_name = OrderedDict(zip(workbook.sheetnames,
                                                     workbook.worksheets))

                # Get list of defined names, grouped by worksheet name.
                defined_names_by_worksheet = \
                    oxh.get_defined_names_by_worksheet(workbook)

                # Select the "Location" entry cell by default.
                workbook.active = 0
                worksheet = worksheets_by_name['Assay Info']
                default_cell = \
                    worksheet[defined_names_by_worksheet['Assay Info']
                              ['DefaultEntry']]
                for attribute_i in ('activeCell', 'sqref'):
                    setattr(worksheet.views.sheetView[0].selection[0],
                            attribute_i, default_cell.coordinate)

                # Set the "Date" entry cell value to the current date.
                date_cell = \
                    worksheet[defined_names_by_worksheet['Assay Info']
                              ['DateEntry']]
                date_cell.value = dt.datetime.now().date()

                # Set the "Date" entry cell value to the current date.
                time_cell = \
                    worksheet[defined_names_by_worksheet['Assay Info']
                              ['TimeEntry']]
                time_cell.value = dt.datetime.now().time()

                # Set the "Laptop" entry cell value to the laptop used.
                laptop_cell = \
                    worksheet[defined_names_by_worksheet['Assay Info']
                              ['LaptopEntry']]
                laptop_cell.value = socket.gethostname()

                # Set the "Box" entry cell value to the box used.
                box_cell = \
                    worksheet[defined_names_by_worksheet['Assay Info']
                              ['BoxEntry']]
                try:
                    box_cell.value = str(dropbot_remote.uuid)
                except:
                    pass
    # Restore the extension lists and data validation definitions to the output
    # workbook (they were removed by `openpyxl`, see above).
    updated_xlsx = oxh.update_extension_lists(output_path, extension_lists)
    with output_path.open('wb') as output:
        output.write(updated_xlsx)
    updated_xlsx = oxh.update_data_validations(output_path, data_validations)
    with output_path.open('wb') as output:
        output.write(updated_xlsx)

    return output_path

class ZikaboxPeripheralBoardPlugin(AppDataController, StepOptionsController,
                                 Plugin):
    '''
    This class is automatically registered with the PluginManager.
    '''
    implements(IPlugin)

    plugin_name = str(ph.path(__file__).realpath().parent.name)
    try:
        version = __version__
    except NameError:
        version = 'v0.0.0+unknown'

    AppFields = Form.of(Float.named('LED 1 brightness')
                        .using(default=0, optional=True,
                               validators=[ValueAtLeast(minimum=0),
                                           ValueAtMost(maximum=1)]),
                        # Float.named('LED 2 brightness')
                        # .using(default=0, optional=True,
                        #        validators=[ValueAtLeast(minimum=0),
                        #                    ValueAtMost(maximum=1)]),
                        Boolean.named('Show Report')
                        .using(default=False, optional=True),
                        Integer.named('Active Heater')
                        .using(default=1, optional=True,
                               validators=[ValueAtLeast(minimum=1),
                                           ValueAtMost(maximum=2)]),
                        Integer.named('Active Thermistor')
                        .using(default=1, optional=True,
                               validators=[ValueAtLeast(minimum=1),
                                           ValueAtMost(maximum=4)]),
                        Boolean.named('Auxiliary Heating')
                        .using(default=False, optional=True),
                        Integer.named('Auxiliary Heater')
                        .using(default=2, optional=True,
                               validators=[ValueAtLeast(minimum=1),
                                           ValueAtMost(maximum=2)]),
                        Integer.named('Auxiliary Thermistor')
                        .using(default=2, optional=True,
                               validators=[ValueAtLeast(minimum=1),
                                           ValueAtMost(maximum=4)]))

    StepFields = Form.of(Boolean.named('Magnet')
                         .using(default=False, optional=True),
                         # Heater Fields
                         Boolean.named('Heater')
                         .using(default=False, optional=True),

                         Float.named('Heater_temperature')
                         .using(default=40, optional=True,
                                validators=[ValueAtLeast(minimum=20),
                                            ValueAtMost(maximum=100)]))

    def __init__(self):
        super(ZikaboxPeripheralBoardPlugin, self).__init__()
        self.board = None
        # XXX `name` attribute is required in addition to `plugin_name`
        #
        # The `name` attribute is required in addition to the `plugin_name`
        # attribute because MicroDrop uses it for plugin labels in, for
        # example, the plugin manager dialog.
        self.name = self.plugin_name

        # Flag to indicate whether user has already been warned about the board
        # not being connected when trying to set board state.
        self._user_warned = False

        # `dropbot.SerialProxy` instance
        self.dropbot_remote = None

        # Latch to, e.g., config menus, only once
        self.initialized = False

        # heater report dialog
        self.hrd = None

    def reset_board_state(self):
        '''
        Reset Zika-Box peripheral board to default state.
        '''
        # Reset user warned state (i.e., warn user next time board settings
        # are applied when board is not connected).
        self._user_warned = False

        if self.board is None:
            return

        with self.board.proxy_lock:
            # Reset the Heater
            self.board.pid_reset()
            # Home the magnet z-stage.
            self.board.zstage.home()

            if not self.board.zstage.is_down:
                logger.warning('Unable to verify z-stage is in homed position.')

        self.dtnow = dt.datetime.now().strftime("%d_%m_%y_%H_%M")

        self.heater_report_dialog()

        # self.update_leds()
        # Turn on the LEDs
        # self.board.led1.on = True
        # self.board.led2.on = True

    def apply_step_options(self, step_options):
        '''
        Apply the specified step options.

        Parameters
        ----------
        step_options : dict
            Dictionary containing the Zika-Box peripheral board plugin options
            for a protocol step.
        '''
        app = get_app()
        app_values = self.get_app_values()

        if self.board:
            step_log = {}

            # log environmental data from peripheral board
            try:
                enum = 0
                while self.board.measure_temperature(5)>=125:
                    if enum >10:
                        break
                temperature = self.board.measure_temperature(5)
                humidity = self.board.measure_temperature(6)

                step_log['environment'] = {'temperature_celsius':temperature,
                                           'relative_humidity':humidity}
                logger.info('Temp= %.1fC, Rel. humidity= %.1f%%' %
                            (temperature,humidity))
            except Exception:
                logger.debug('[%s] Failed to get environment data.', __name__,
                             exc_info=True)

            # Save state of LEDs
            led1_on = self.board.led1.on
            # led2_on = self.board.led2.on

            services_by_name = {service_i.name: service_i
                                for service_i in
                                PluginGlobals
                                .env('microdrop.managed').services}

            step_label = None
            if 'step_label_plugin' in services_by_name:
                # Step label is set for current step
                step_label_plugin = (services_by_name
                                     .get('step_label_plugin'))
                step_label = (step_label_plugin.get_step_options()
                              or {}).get('label')

            # Apply board hardware options.
            try:
                # Magnet z-stage
                # --------------
                with self.board.proxy_lock:
                    if step_options.get('Magnet'):
                        # Send board request to move magnet to position (if it is
                        # already engaged, this function does nothing).
                        self.board.zstage.up()
                    else:
                        # Send board request to move magnet to down position (if
                        # it is already engaged, this function does nothing).
                        # Move to low position and then home
                        # used to save time and avoid magnet going beyong the
                        # endstop and loosing steps
                        if not self.board.zstage.is_down:
                            self.board.zstage.move_to(1)
                            self.board.zstage.home()

                # Heater
                # -------
                if step_options.get('Heater'):
                    _heater = app_values.get('Active Heater')
                    _thermistor = app_values.get('Active Thermistor')
                    heater_target = step_options.get('Heater_temperature')

                    with self.board.proxy_lock:
                        if heater_target > 20 :
                            self.board.autopid_activate(_heater, _thermistor)
                            self.board.set_heater_target(_heater, heater_target)
                            if (app_values.get('Auxiliary Heating')):
                                aux_heater = app_values.get('Auxiliary Heater')
                                aux_thermistor = app_values.get('Auxiliary Thermistor')
                                self.board.set_heater_target(aux_heater, heater_target)
                                self.board.autopid_aux_activate(aux_heater,
                                                                aux_thermistor)
                        else:
                            self.board.autopid_deactivate()


            except Exception:
                logger.error('[%s] Error applying step options.', __name__,
                             exc_info=True)
            # finally:
                # self.board.led1.on = led1_on
                # self.board.led2.on = led2_on

                app.experiment_log.add_data(step_log, self.name)

        elif not self._user_warned:
            logger.warning('[%s] Cannot apply board settings since board is '
                           'not connected.', __name__, exc_info=True)
            # Do not warn user again until after the next connection attempt.
            self._user_warned = True

    def json_to_excel(self):
    #     '''
    #     Make an excel file from the JSON file
    #     where the temperatures are stored
        app = get_app()
        log_dir = app.experiment_log.get_log_path()

        data_path = log_dir.joinpath('Temperature_log.ndjson')
        if os.path.exists(data_path) == False:
            logger.info('No data to export')
            return

        data = {}
        with open(data_path) as json_file:
            for line in json_file:
                data_json_ij = json.loads(line)
                for key in data_json_ij.keys():
                    try:
                        if key == 'data':
                            data[key].append(data_json_ij[key][0])
                        elif key == 'index':
                            data[key].append(data_json_ij[key])
                    except KeyError:
                        data[key] = []
                        if key == 'data':
                            data[key].append(data_json_ij[key][0])
                        elif key == 'index':
                            data[key].append(data_json_ij[key])

        data['columns'] = data_json_ij['columns']
        data['index'] = np.array(data['index']).flatten()

        df = pd.DataFrame(data['data'],
                  columns = data['columns'],
                  index = data['index'])
        df.index.rename('Time', inplace=True)
        df['DeltaTime(s)'] = (df.index - df.index[0])/1000.0
        df.index = pd.DatetimeIndex(df.index*1e6)
        df = df.reset_index().set_index(['Time','DeltaTime(s)'])

        # output_path = log_dir.joinpath('Temperature_data.xlsx')
        output_path = log_dir.joinpath('Experiment_log_{}.xlsx'.format(self.dtnow))

        # Open template workbook to modify it in-memory before writing to the
        # output file.
        with contextlib.closing(ox.load_workbook(output_path)) as workbook:
            # Create pandas Excel writer to make it easier to append data
            # frames as worksheets.
            with pd.ExcelWriter(output_path, engine='openpyxl') as writer:
                # Configure pandas Excel writer to append to template workbook
                # contents.
                writer.book = workbook

                if 'Temperature_log' in workbook.get_sheet_names():
                    sheet = workbook.get_sheet_by_name('Temperature_log')
                    workbook.remove_sheet(sheet)
                df.to_excel(writer, sheet_name='Temperature_log')
                writer.save()
                writer.close()


    def create_excel_log(self):
        '''
        Create output Excel Experiment Log.
        '''
        app = get_app()
        log_dir = app.experiment_log.get_log_path()

        # Update Excel log file
        output_path = log_dir.joinpath('Experiment_log_{}.xlsx'.format(self.dtnow))

        self.prompt_closed = threading.Event()

        @gtk_threadsafe
        def window():
            response = yesno('Error writing Experiment Log to Excel '
                             'spreadsheet output path: `%s`.\n\nTry '
                             'again?')
            if response == gtk.RESPONSE_NO:
                self.prompt_closed.set()

        while True:
            try:
                _write_results(TEMPLATE_PATH, output_path, self.dropbot_remote)
                self.prompt_closed.set()
                break
            except IOError:
                window()
                self.prompt_closed.wait()
                break


    def launch_log(self):
        app = get_app()
        log_dir = app.experiment_log.get_log_path()

        # Update Excel log file
        output_path = log_dir.joinpath('Experiment_log_{}.xlsx'.format(self.dtnow))
        output_path.launch()

    @gtk_threadsafe
    def heater_report_dialog(self):
        if self.hrd is None:
            self.hrd = monitor_dialog._dialog(self.board)

    def open_board_connection(self):
        '''
        Establish serial connection to Zika-Box peripheral board.
        '''
        # Try to connect to peripheral board through serial connection.

        # XXX Try to connect multiple times.
        # See [issue 1][1] on the [Zika-Box peripheral board firmware
        # project][2].
        #
        # [1]: https://github.com/wheeler-microfluidics/Zika-Box-peripheral-board.py/issues/1
        # [2]: https://github.com/wheeler-microfluidics/Zika-Box-peripheral-board.py
        retry_count = 2
        for i in xrange(retry_count):
            try:
                self.board.close()
                self.board = None
            except Exception:
                pass

            try:
                self.board = zikabox.SerialProxy()
                self.board.proxy_lock = threading.RLock()

                host_software_version = utility.Version.fromstring(
                    str(self.board.host_software_version))
                remote_software_version = utility.Version.fromstring(
                    str(self.board.remote_software_version))

                # Offer to reflash the firmware if the major and minor versions
                # are not not identical. If micro versions are different,
                # the firmware is assumed to be compatible. See [1]
                #
                # [1]: https://github.com/wheeler-microfluidics/base-node-rpc/
                #              issues/8
                if any([host_software_version.major !=
                        remote_software_version.major,
                        host_software_version.minor !=
                        remote_software_version.minor]):
                    response = yesno("The Zika-Box peripheral board firmware "
                                     "version (%s) does not match the driver "
                                     "version (%s). Update firmware?" %
                                     (remote_software_version,
                                      host_software_version))
                    if response == gtk.RESPONSE_YES:
                        self.on_flash_firmware()

                # Serial connection to peripheral **successfully established**.
                logger.info('Serial connection to peripheral board '
                            '**successfully established** on port `%s`',
                            self.board.port)
                logger.info('Peripheral board properties:\n%s',
                            self.board.properties)
                logger.info('Reset board state to defaults.')
                break
            except (serial.SerialException, IOError):
                time.sleep(1)
        else:
            # Serial connection to peripheral **could not be established**.
            logger.warning('Serial connection to peripheral board could not '
                           'be established.')

    def on_edit_configuration(self, widget=None, data=None):
        '''
        Display a dialog to manually edit the configuration settings.
        '''
        config = self.board.config
        form = dict_to_form(config)
        dialog = FormViewDialog(form, 'Edit configuration settings')
        valid, response = dialog.run()
        if valid:
            response.pop('id', None)
            self.board.update_config(**response)
            self.board.pid_req.update_from_EEPROM_values()

    def on_flash_firmware(self, widget=None, data=None):
        app = get_app()

        self.prompt_closed = threading.Event()

        @gtk_threadsafe
        def window():
            app.main_window_controller.info("Firmware updated successfully.",
                                            "Firmware update")
            self.prompt_closed.set()

        try:
            self.board.flash_firmware()
            window()
            self.prompt_closed.wait()
        except Exception, why:
            logger.error("Problem flashing firmware. ""%s" % why)
            self.prompt_closed.set()

    @gtk_threadsafe
    def close_board_connection(self):
        '''
        Close serial connection to Zika-Box peripheral board.
        '''
        if self.board is not None:
            # Destroy heater report dialog
            if self.hrd is not None:
                self.hrd.destroy()
                self.hrd = None
            try:
                self.board.autopid_deactivate()
                logger.info('Heaters turned off.')

                self.board.zstage.home()
                logger.info('Z-stage homed.')
            except:
                pass
            # Close board connection and release serial connection.
            self.board.close()


    def get_schedule_requests(self, function_name):
        """
        Returns a list of scheduling requests (i.e., ScheduleRequest
        instances) for the function specified by function_name.
        """
        # TODO: this should be re-enabled once we can get the
        # Zika-Box-peripheral-board to connect **after** the Dropbot.
        if function_name in ['on_plugin_enable']:
           return [ScheduleRequest('dropbot_plugin', self.name)]
        return []

    ###########################################################################
    # MicroDrop pyutillib plugin signal handlers
    # ------------------------------------------
    ###########################################################################
    def on_plugin_enable(self):
        '''
        Handler called when plugin is enabled.

        For example, when the MicroDrop application is **launched**, or when
        the plugin is **enabled** from the plugin manager dialog.
        '''
        # self.initialize_connection_with_dropbot()
        self.dropbot_remote = None

        self.open_board_connection()
        if not self.initialized:
            app = get_app()
            self.tools_menu_item = gtk.MenuItem("Zika-Box")
            app.main_window_controller.menu_tools.append(self.tools_menu_item)
            self.tools_menu = gtk.Menu()
            self.tools_menu_item.set_submenu(self.tools_menu)

            self.edit_config_menu_item = \
                gtk.MenuItem("Edit configuration settings...")
            self.tools_menu.append(self.edit_config_menu_item)
            self.edit_config_menu_item.connect("activate",
                                               self.on_edit_configuration)
            self.edit_config_menu_item.show()
            self.initialized = True

        # if we're connected to the board, display the menu
        try:
            if self.board:
                self.reset_board_state()
                self.tools_menu.show()
                self.tools_menu_item.show()

            super(ZikaboxPeripheralBoardPlugin, self).on_plugin_enable()
        except AttributeError:
            pass

    def initialize_connection_with_dropbot(self):
        '''
        If the dropbot plugin is installed and enabled, try getting its
        reference.
        '''
        try:
            service = get_service_instance_by_name('dropbot_plugin')
            if service.enabled():
                self.dropbot_remote = service.control_board
                assert(self.dropbot_remote.properties.package_name == 'dropbot')
            else:
                self.dropbot_remote = None
        except Exception:
            logger.debug('[%s] Could not communicate with Dropbot.', __name__,
                         exc_info=True)
            logger.warning('Could not communicate with DropBot.')

        # try:
        #     if self.dropbot_remote:
        #         env = self.dropbot_remote.get_environment_state()
        #         logger.info('temp=%.1fC, Rel. humidity=%.1f%%' %
        #                     (env['temperature_celsius'],
        #                      100 * env['relative_humidity']))
        # except Exception:
        #     logger.warning('Could not get temperature/humidity data.')

    def on_plugin_disable(self):
        '''
        Handler called when plugin is disabled.

        For example, when the MicroDrop application is **closed**, or when the
        plugin is **disabled** from the plugin manager dialog.
        '''
        try:
            super(ZikaboxPeripheralBoardPlugin, self).on_plugin_disable()
        except AttributeError:
            pass
        self.close_board_connection()
        self.tools_menu.hide()
        self.tools_menu_item.hide()

    def on_protocol_run(self):
        '''
        Handler called when a protocol starts running.
        '''
        # TODO: this should be run in on_plugin_enable; however, the
        # Zika-Box-peripheral-board seems to have trouble connecting **after**
        # the DropBot has connected.
        if not self.dropbot_remote:
            self.initialize_connection_with_dropbot()

        app = get_app()
        log_dir = app.experiment_log.get_log_path()
        output_path = log_dir.joinpath('Experiment_log_{}.xlsx'.format(self.dtnow))
        if os.path.exists(output_path) == False:
            self.create_excel_log()

    def on_protocol_pause(self):
        '''
        Handler called when a protocol is paused.
        '''

    def on_protocol_finished(self):
        # Protocol has finished.  Update
        app_values = self.get_app_values()
        self.json_to_excel()
        if app_values.get('Show Report'):
            self.launch_log()


    def on_experiment_log_changed(self, experiment_log):
        '''
        Handler called when a new experiment starts.

        .. versionchanged:: v0.23.2
            Fix typo to retrieve :data:`app_values` before item lookup.
        '''
        try:
            if self.board:
                self.reset_board_state()
                logger.info('Reset board state to defaults.')
        except AttributeError:
            logger.info('Board not connected.')
            pass

    def on_app_options_changed(self, plugin_name):
        """
        Handler called when the app options are changed for a particular
        plugin.  This will, for example, allow for GUI elements to be
        updated.

        Parameters
        ----------
        plugin : str
            Plugin name for which the app options changed
        """
        if plugin_name == self.name and self.board:
            self.update_leds()

    def update_leds(self):
        app_values = self.get_app_values()

        # logger.info(app_values)

        # for k, v in app_values.items():
        #     if k == 'LED 1 brightness':
        #         self.board.led1.brightness = v
        #     elif k == 'LED 2 brightness':
        #         self.board.led2.brightness = v

#     def on_step_options_changed(self, plugin, step_number):
#         '''
#         Handler called when field values for the specified plugin and step.
#
#         Parameters
#         ----------
#         plugin : str
#             Name of plugin.
#         step_number : int
#             Step index number.
#         '''
#         # Step options have changed.
#         app = get_app()
#
#         if all([plugin == self.plugin_name, app.realtime_mode,
#                 step_number == app.protocol.current_step_number]):
#             # Apply step options.
#             options = self.get_step_options()
#             self.apply_step_options(options)

    def on_app_exit(self):
        self.close_board_connection()

    @asyncio.coroutine
    def on_step_run(self, plugin_kwargs, signals):
        '''
        Handler called whenever a step is executed.

        Plugins that handle this signal **MUST** emit the ``on_step_complete``
        signal once they have completed the step.  The protocol controller will
        wait until all plugins have completed the current step before
        proceeding.
        '''
        # Get latest step field values for this plugin.
        options = plugin_kwargs[self.name]
        # Apply step options
        self.apply_step_options(options)

#     def on_step_swapped(self, original_step_number, new_step_number):
#         '''
#         Handler called when a new step is activated/selected.
#
#         Parameters
#         ----------
#         original_step_number : int
#             Step number of previously activated step.
#         new_step_number : int
#             Step number of newly activated step.
#         '''
#         # Step options have changed.
#         app = get_app()
#         if app.realtime_mode and not app.running:
#             # Apply step options.
#             options = self.get_step_options()
#             self.apply_step_options(options)


PluginGlobals.pop_env()
